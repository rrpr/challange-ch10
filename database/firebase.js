import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyDgA7y8FlVubOFZV4VmNy_GyKFvyxYCU9M",
    authDomain: "belajar-firebase-67105.firebaseapp.com",
    projectId: "belajar-firebase-67105",
    storageBucket: "belajar-firebase-67105.appspot.com",
    messagingSenderId: "1005294042598",
    appId: "1:1005294042598:web:6eda7166a6e4dfeb6b6a27"
};

const app = initializeApp(firebaseConfig);

export const authInstance = getAuth(app);