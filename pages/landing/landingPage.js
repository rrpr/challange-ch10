import { SliderSponsor } from "../../components/slider";
import Image from "next/image";
import Link from "next/link";

export default function Landing() {
  return (
    <div className="bg-white">
      <main>
        {/* Hero section */}
        <div className="relative">
          <div className="absolute inset-x-0 bottom-0 h-1/2" />
          <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div className="relative shadow-xl sm:rounded-2xl sm:overflow-hidden">
              <div className="absolute inset-0">
                <Image
                  fill
                  className="h-full w-full object-cover"
                  src="/images/game-night-essentials.jpeg"
                  alt="People happy playing together"
                />
                <div className="absolute inset-0 bg-gradient-to-r from-slate-500 to-slate-700 mix-blend-multiply" />
              </div>
              <div className="relative px-4 py-16 sm:px-6 sm:py-24 lg:py-32 lg:px-8">
                <h1 className="text-center text-4xl font-extrabold tracking-tight sm:text-5xl lg:text-6xl">
                  <span className="block text-white">Binar Academy FSW</span>
                  <span className="block text-red-500">Challange 10</span>
                </h1>
                <p className="mt-6 max-w-lg mx-auto text-center text-xl text-red-200 sm:max-w-3xl">
                  Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure
                  qui lorem cupidatat commodo. Elit sunt amet fugiat veniam
                  occaecat fugiat aliqua.
                </p>
                <div className="mt-6 max-w-sm mx-auto sm:max-w-none sm:flex sm:justify-center">
                  <div className="space-y-4 sm:space-y-0 sm:mx-auto sm:inline-grid sm:grid-cols-2 sm:gap-5">
                    <Link
                      href="/game"
                      className="flex items-center justify-center px-4 py-3 border border-transparent text-base font-medium rounded-md shadow-sm text-red-700 bg-white hover:bg-red-50 sm:px-8"
                    >
                      Lihat Game
                    </Link>
                    <Link
                      href="#"
                      onClick={()=>{ alert('maintenance'); }}
                      className="flex items-center justify-center px-4 py-3 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-red-500 bg-opacity-60 hover:bg-opacity-70 sm:px-8"
                    >
                      Mainkan Sekarang
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Logo Cloud Belum Slider*/}
        <div className="bg-gray-100 mt-20">
          <div className="max-w-7xl mx-auto py-16 px-4 sm:px-6 lg:px-8">
            <p className="text-center text-sm mb-8 font-semibold uppercase text-gray-500 tracking-wide">
              Sponsor
            </p>
            <SliderSponsor />
          </div>
        </div>

        {/* CTA Section */}
        <div className="bg-white">
          <div className="max-w-4xl mx-auto py-16 px-4 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8 lg:flex lg:items-center lg:justify-between">
            <h2 className="text-4xl font-extrabold tracking-tight text-gray-900 sm:text-4xl">
              <span className="block">Sudah Siap Menerima Tantangan?</span>
              <span className="-mb-1 pb-1 block bg-gradient-to-r from-rose-500 to-red-600 bg-clip-text text-transparent">
                Aliquam massa turpis.
              </span>
            </h2>
            <div className="mt-6 space-y-4 sm:space-y-0 sm:flex sm:space-x-5">
              <Link
                href="#"
                onClick={() => { alert('maintenance') }}
                className="flex items-center justify-center bg-gradient-to-r from-rose-500 to-red-600 bg-origin-border px-4 py-3 border border-transparent text-base font-medium rounded-md shadow-sm text-white hover:from-rose-700 hover:to-red-800"
              >
                Mainkan Sekarang
              </Link>
              <Link
                href="#"
                onClick={() => { alert('maintenance') }}
                className="flex items-center justify-center px-4 py-3 border border-transparent text-base font-medium rounded-md shadow-sm text-red-800 bg-red-50 hover:bg-red-100"
              >
                Daftar Akun
              </Link>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
