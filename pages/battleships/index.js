import { useState, useEffect } from "react";

export default function RandomNumberPage() {
  const [history, setHistory] = useState([]);
  const [randomNumber, setRandomNumber] = useState(null);

  
  useEffect(() => {
    const storedHistory = localStorage.getItem("history");
    if (storedHistory) {
      setHistory(JSON.parse(storedHistory));
    }
  }, []);

 
  useEffect(() => {
    localStorage.setItem("history", JSON.stringify(history));
  }, [history]);

  const handleRoll = () => {
    const newNumber = Math.floor(Math.random() * 10) + 1;
    setRandomNumber(newNumber);
    setHistory([...history, newNumber]);
  };

  return (
    <div className="d-flex justify-content-center">
      <h1>Skor anda</h1>
      <p>Klik untuk mendapatkan skor random.</p>
      <button className="btn btn-dark" onClick={handleRoll}>
        Roll
      </button>
      {randomNumber && (
        <p>
          Skor anda: <strong>{randomNumber}</strong>
        </p>
      )}
      <h2>Histori Skor</h2>
      <ul>
        {history.map((number, index) => (
          <li key={index}>{number}</li>
        ))}
      </ul>
    </div>
  );
}