import React, { useState } from 'react';
import { useAuth } from '../../context/auth';
import Link from "next/link";

const Login = (props) => {
  const { user, signin } = useAuth();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleSignin = () => {
      signin(email, password);
      
  };

  return (
    <div>
        <span>Email : </span>
        <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="email"/> <br/>
        <span>Password : </span>
        <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="password"/> <br/>
        <button onClick={handleSignin} >Login</button>

    </div>
  )
}

export default Login
