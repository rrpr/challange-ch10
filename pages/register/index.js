import { FirebaseError } from 'firebase/app';
import React, { useState, useCallback } from 'react'
import { useAuth } from '../../context/auth';

export default function Register() {
  const { signup } = useAuth();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleClickResigter = useCallback(async () => {
      await signup(email, password);
  }, [email, password,signup]) 
  
  return (
    <div>
        <span>Email : </span>
        <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="email"/> <br/>
        <span>Password : </span>
        <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="password"/> <br/>
        <button onClick={handleClickResigter} >Register</button>
    </div>
  )
}
