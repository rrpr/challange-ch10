import '../styles/globals.css'
import Head from 'next/head'
import Layout from '../components/layout'
import { AuthProvider } from '../context/auth'

function MyApp({ Component, pageProps }) {
  console.log(pageProps);

  return (
    <AuthProvider>
    <Layout>
      <Component {...pageProps} />
    </Layout>
    </AuthProvider>
  )
}

export default MyApp

