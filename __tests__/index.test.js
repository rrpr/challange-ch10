import React from "react";
// import { render, screen } from "../node_modules/react-dom/test-utils";
import { render, screen } from '@testing-library/react'
import HomePage from "../pages/index";
import ReactTestUtils from 'react-dom/test-utils'

describe("HomePage", () => {
  it("should render the heading", () => {
    const textToFind = "Binar Academy FSW"

    render(<HomePage />);
    const heading = screen.getByText(textToFind);

    expect(heading).toBeInTheDocument();
  });
});